# 15 - Configuration Management with Ansible - Automate Kubernetes Deployment

**Demo Project:**
Automate Kubernetes Deployment

**Technologies used:**
Ansible, Terraform, Kubernetes, AWS EKS, Python, Linux

**Project Description:**
- Create EKS cluster with Terraform
- Write Ansible Play to deploy application in a new K8s namespace



15 - Configuration Management with Ansible
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp





